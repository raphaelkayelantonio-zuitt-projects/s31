const express = require("express");
const taskController = require("../controllers/taskController");

const router = express.Router();

router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


router.post("/", (req, res) =>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

router.delete("/:id", (req, res) =>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


router.patch("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/:id", (req, res) =>{
	taskController.getspecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {
	taskControllers.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
}) 


module.exports = router;